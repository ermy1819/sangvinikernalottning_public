﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SangvinikernaLottning
{
    internal interface ITeamMember
    {
        string Name { get; }

        ISchedule Schedule { get; set; }
        int InitialBrokenRules { get; set; }

        int RequestedNumberOfMatches { get; set; }

        bool IsSchedulePossible();

        int GetBrokenRuleScore();
        int GetNumberOfBrokenRules();
    }

    internal abstract class TeamMember : ITeamMember
    {
        public string Name => GetType().Name;

        public ISchedule Schedule { get; set; }
        public int InitialBrokenRules { get; set; }
        public int RequestedNumberOfMatches { get; set; } = -1;

        public int GetBrokenRuleScore()
        {
            var brokenRules = GetNumberOfBrokenRules();
            return (brokenRules * brokenRules * 1000000) / (InitialBrokenRules * InitialBrokenRules);
        }

        public int GetNumberOfBrokenRules()
        {
            var numberOfMatches = Schedule.Days.Count(d => IsScheduledToPlay(d) > 0);
            var matchError = Math.Abs(numberOfMatches - RequestedNumberOfMatches);
            return matchError + GetNumberOfBrokenPlayerSpecificRules();
        }

        public bool IsSchedulePossible()
        {
            var impossibleDates = ImpossibleDates.ToList();

            foreach(var day in Schedule.Days)
            {
                int count = IsScheduledToPlay(day);
                if (count > 1)
                    return false;

                if (count == 1 && impossibleDates.Contains(day.Date))
                    return false;
            }

            return IsSchedulePossibleByPlayerSpecificRules();
        }

        protected int IsScheduledToPlay(IPlayingDay day)
        {
            var count = day.Pair1.Item1 == this ? 1 : 0;
            count += day.Pair1.Item2 == this ? 1 : 0;
            count += day.Pair2.Item1 == this ? 1 : 0;
            count += day.Pair2.Item2 == this ? 1 : 0;
            return count;
        }

        protected ITeamMember PartnerOnPlannedDay(IPlayingDay day)
        {
            if (day.Pair1.Item1 == this)
                return day.Pair1.Item2;
            if (day.Pair1.Item2 == this)
                return day.Pair1.Item1;
            if (day.Pair2.Item1 == this)
                return day.Pair2.Item2;
            if (day.Pair2.Item2 == this)
                return day.Pair2.Item1;

            return null;
        }

        protected abstract int GetNumberOfBrokenPlayerSpecificRules();

        protected virtual bool IsSchedulePossibleByPlayerSpecificRules() => true;

        protected virtual IEnumerable<DateTime> ImpossibleDates { get { yield break; } }
    }
}