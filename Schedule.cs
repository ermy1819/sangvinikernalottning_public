﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SangvinikernaLottning
{
    class Schedule : ISchedule
    {
        public IList<IWeek> Weeks { get; set; }
        public IEnumerable<IPlayingDay> Days => Weeks.Select(w => w.Monday).Concat(Weeks.Select(w => w.Tuesday));

        public IList<ITeamMember> TeamMembers { get; set; }

        public int GetTotalScore()
        {
            return TeamMembers.Sum(m => m.GetBrokenRuleScore());
        }

        public bool IsPossible()
        {
            return TeamMembers.All(m => m.IsSchedulePossible());
        }

        public override string ToString()
        {
            var builder = new StringBuilder();

            foreach (var week in Weeks)
            {
                builder.AppendLine(week.Monday.ToString());
                builder.AppendLine(week.Tuesday.ToString());
            }

            return builder.ToString();
        }
    }
}
