﻿using SangvinikernaLottning.TeamMembers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SangvinikernaLottning
{
    class CreateSchedule2019
    {
        public ISchedule CreateSchedule()
        {
            var schedule = new Schedule()
            {
                TeamMembers = new List<ITeamMember>
                {
                    new Calle(),
                    new Erik(),
                    new Jan(),
                    new Joakim(),
                    new Klara(),
                    new Magnus(),
                    new Maria(),
                    new Mikaela(),
                    new Nillan(),
                    new Tobias()
                },
                Weeks = new List<IWeek>
                {
                    new Week
                    {
                        WeekNumber = 33,
                        Monday = new Monday { Date = new DateTime(2019, 08, 12)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 08, 13)}
                    },
                    new Week
                    {
                        WeekNumber = 34,
                        Monday = new Monday { Date = new DateTime(2019, 08, 19)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 08, 20)}
                    },
                    new Week
                    {
                        WeekNumber = 35,
                        Monday = new Monday { Date = new DateTime(2019, 08, 26)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 08, 27)}
                    },
                    new Week
                    {
                        WeekNumber = 36,
                        Monday = new Monday { Date = new DateTime(2019, 09, 02)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 09, 03)}
                    },
                    new Week
                    {
                        WeekNumber = 37,
                        Monday = new Monday { Date = new DateTime(2019, 09, 09)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 09, 10)}
                    },
                    new Week
                    {
                        WeekNumber = 38,
                        Monday = new Monday { Date = new DateTime(2019, 09, 16)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 09, 17)}
                    },
                    new Week
                    {
                        WeekNumber = 39,
                        Monday = new Monday { Date = new DateTime(2019, 09, 23)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 09, 24)}
                    },
                    new Week
                    {
                        WeekNumber = 40,
                        Monday = new Monday { Date = new DateTime(2019, 09, 30)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 10, 01)}
                    },
                    new Week
                    {
                        WeekNumber = 41,
                        Monday = new Monday { Date = new DateTime(2019, 10, 07)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 10, 08)}
                    },
                    new Week
                    {
                        WeekNumber = 42,
                        Monday = new Monday { Date = new DateTime(2019, 10, 14)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 10, 15)}
                    },
                    new Week
                    {
                        WeekNumber = 43,
                        Monday = new Monday { Date = new DateTime(2019, 10, 21)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 10, 22)}
                    },
                    new Week
                    {
                        WeekNumber = 44,
                        Monday = new Monday { Date = new DateTime(2019, 10, 28)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 10, 29)}
                    },
                    new Week
                    {
                        WeekNumber = 45,
                        Monday = new Monday { Date = new DateTime(2019, 11, 4)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 11, 5)}
                    },
                    new Week
                    {
                        WeekNumber = 46,
                        Monday = new Monday { Date = new DateTime(2019, 11, 11)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 11, 12)}
                    },
                    new Week
                    {
                        WeekNumber = 47,
                        Monday = new Monday { Date = new DateTime(2019, 11, 18)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 11, 19)}
                    },
                    new Week
                    {
                        WeekNumber = 48,
                        Monday = new Monday { Date = new DateTime(2019, 11, 25)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 11, 26)}
                    },
                    new Week
                    {
                        WeekNumber = 49,
                        Monday = new Monday { Date = new DateTime(2019, 12, 02)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 12, 03)}
                    },
                    new Week
                    {
                        WeekNumber = 50,
                        Monday = new Monday { Date = new DateTime(2019, 12, 09)},
                        Tuesday = new Tuesday { Date = new DateTime(2019, 12, 10)}
                    },
                }
            };

            foreach(var week in schedule.Weeks)
            {
                week.Monday.ContainingWeek = week;
                week.Tuesday.ContainingWeek = week;
            }

            var membersWithRestrictedPlay = schedule.TeamMembers.Where(m => m.RequestedNumberOfMatches != -1).ToList();
            var membersWithUnrestrictedPlay = schedule.TeamMembers.Where(m => m.RequestedNumberOfMatches == -1).ToList();
            var availableMatches = 18 * 2 * 4 - membersWithRestrictedPlay.Sum(m => m.RequestedNumberOfMatches);
            var matchesPerPlayer = availableMatches / membersWithUnrestrictedPlay.Count;
            foreach(var member in membersWithUnrestrictedPlay)
            {
                member.RequestedNumberOfMatches = matchesPerPlayer;
            }

            foreach (var member in membersWithRestrictedPlay)
            {
                member.RequestedNumberOfMatches = Math.Min(member.RequestedNumberOfMatches, matchesPerPlayer);
            }

            foreach (var member in schedule.TeamMembers)
            {
                member.Schedule = schedule;
                member.InitialBrokenRules = member.GetNumberOfBrokenRules();
            }

            return schedule;
        }
    }
}
