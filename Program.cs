﻿using System;
using System.IO;

namespace SangvinikernaLottning
{
    class Program
    {
        [STAThread]
        static void Main(string[] args)
        {
            var bestSchedule = int.MaxValue;
            var random = new Random((int)DateTime.Now.Ticks);
            while (true)
            {
                try
                {
                    var schedule = new CreateSchedule2019().CreateSchedule();
                    var planner = new Planner();
                    planner.Random = random;
                    planner.FillSchedule(schedule);
                    var currentScore = schedule.GetTotalScore();
                    Console.WriteLine("Found feasible schedule with ranking " + currentScore);
                    var intermediateScore = currentScore;

                    do
                    {
                        currentScore = intermediateScore;
                        planner.ImproveByReplacingPlayers(schedule);
                        Console.WriteLine("Improved by replacing players " + schedule.GetTotalScore());
                        planner.ImproveBySwitchingPlayers(schedule);
                        Console.WriteLine("Improved by swithing players " + schedule.GetTotalScore());
                        planner.ImproveBySwithingPairs(schedule);
                        intermediateScore = schedule.GetTotalScore();
                        Console.WriteLine("Improved by swithing pairs " + intermediateScore);
                    } while (intermediateScore < currentScore);

                    currentScore = intermediateScore;

                    if (currentScore < bestSchedule)
                    {
                        bestSchedule = currentScore;
                        Console.WriteLine("New best schedule found!");
                        Console.WriteLine(schedule);
                        File.AppendAllText("bestschedule.log", Environment.NewLine + Environment.NewLine + "New best schedule found! " + schedule.GetTotalScore() + Environment.NewLine + schedule.ToString());
                    }
                }
                catch (NoFeasibleScheduleFound)
                {
                    Console.WriteLine("No Schedule found!");
                }

                Console.WriteLine("");
                Console.WriteLine("");
            }
        }
    }
}
