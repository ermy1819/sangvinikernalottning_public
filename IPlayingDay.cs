﻿using System;

namespace SangvinikernaLottning
{
    interface IPlayingDay
    {
        DateTime Date { get; set; }
        (ITeamMember, ITeamMember) Pair1 { get; set; }
        (ITeamMember, ITeamMember) Pair2 { get; set; }

        IWeek ContainingWeek { get; set; }           

        bool IsEquivalent(PlayingDay equivalent);
        bool IsPlanned { get; }
    }
}