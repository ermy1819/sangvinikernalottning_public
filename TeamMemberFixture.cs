﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SangvinikernaLottning
{
    class TeamMemberFixture
    {
        [Test]
        public void ShouldNotBePossibleDayWhenDayIsPlanned()
        {
            var plannedDay = Substitute.For<IPlayingDay>();
            plannedDay.IsPlanned.Returns(true);

            var schedule = Substitute.For<ISchedule>();
            schedule.Days.Returns(new List<IPlayingDay> { plannedDay });

            var sut = Substitute.For<TeamMember>();

            //Assert.That(sut.IsDayPrefered(plannedDay, schedule), Is.False);
            //Assert.That(sut.PreferedDaysLeft(schedule), Is.EqualTo(0));
        }
    }
}
