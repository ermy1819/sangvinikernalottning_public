﻿using System;
using System.Collections.Generic;

namespace SangvinikernaLottning
{
    static class Shuffler
    {
        public static IList<T> Shuffle<T>(this IList<T> list, Random rnd)
        {
            for (var i = 0; i < list.Count - 1; i++)
                list.Swap(i, rnd.Next(i, list.Count));

            return list;
        }

        public static void Swap<T>(this IList<T> list, int i, int j)
        {
            var temp = list[i];
            list[i] = list[j];
            list[j] = temp;
        }

        public static IEnumerable<(T, T)> GetAllPairs<T>(this IList<T> list)
        {
            for (int i = 0; i < list.Count - 1; ++i)
                for (int j = i + 1; j < list.Count; ++j)
                    yield return (list[i], list[j]);
        }

        public static T RandomItem<T>(this IList<T> list, Random random)
        {
            return list[random.Next(list.Count - 1)];
        }
    }
}
