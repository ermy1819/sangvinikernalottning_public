﻿using System.Collections.Generic;

namespace SangvinikernaLottning
{
    interface ISchedule
    {
        IList<IWeek> Weeks { get; }
        IEnumerable<IPlayingDay> Days { get; }
        IList<ITeamMember> TeamMembers { get; set; }

        int GetTotalScore();
        bool IsPossible();
    }
}