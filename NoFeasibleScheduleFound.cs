﻿using System;
using System.Runtime.Serialization;

namespace SangvinikernaLottning
{
    [Serializable]
    internal class NoFeasibleScheduleFound : Exception
    {
        public NoFeasibleScheduleFound()
        {
        }

        public NoFeasibleScheduleFound(string message) : base(message)
        {
        }

        public NoFeasibleScheduleFound(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoFeasibleScheduleFound(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}