﻿namespace SangvinikernaLottning
{
    internal interface IWeek
    {
        IPlayingDay Monday { get; set; }
        IPlayingDay Tuesday { get; set; }

        int WeekNumber { get; set; }
    }

    class Week : IWeek
    {
        public IPlayingDay Monday { get; set; }
        public IPlayingDay Tuesday { get; set; }
        public int WeekNumber { get; set; }
    }
}