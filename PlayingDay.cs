﻿using System;

namespace SangvinikernaLottning
{
    abstract class PlayingDay : IPlayingDay
    {
        public DateTime Date { get; set; }

        public (ITeamMember, ITeamMember) Pair1 { get; set; }

        public (ITeamMember, ITeamMember) Pair2 { get; set; }

        public IWeek ContainingWeek { get; set; }

        public override string ToString()
        {
            return string.Join(";", Date.ToString("MMM dd"), Pair1.Item1.Name, Pair1.Item2.Name, Pair2.Item1.Name, Pair2.Item2.Name);
        }

        public bool IsEquivalent(PlayingDay equivalent)
        {
            return SamePair(Pair1, equivalent.Pair1) && SamePair(Pair2, equivalent.Pair2) ||
                SamePair(Pair1, equivalent.Pair2) && SamePair(Pair2, equivalent.Pair1);
        }

        public bool IsPlanned => Pair1.Item1 != null && Pair1.Item2 != null && Pair2.Item1 != null && Pair2.Item2 != null;

        private static bool SamePair((ITeamMember, ITeamMember) pair1, (ITeamMember, ITeamMember) pair2)
        {
            return pair1.Item1 == pair2.Item1 && pair1.Item2 == pair2.Item2 ||
                pair1.Item2 == pair2.Item1 && pair1.Item1 == pair2.Item2;
        }
    }

    internal class Monday : PlayingDay
    {
    }

    internal class Tuesday : PlayingDay
    {
    }
}
