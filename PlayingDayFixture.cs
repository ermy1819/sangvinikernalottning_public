﻿using NSubstitute;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace SangvinikernaLottning
{
    [TestFixture]
    class PlayingDayFixture
    {
        [Test]
        public void ShouldAddDateAndPlayerNamesInSemicolonSepareatedListWhenTranformingToString()
        {
            var sut = new Monday()
            {
                Date = new DateTime(2019, 07, 13),
                Pair1 = (Substitute.For<ITeamMember>(), Substitute.For<ITeamMember>()),
                Pair2 = (Substitute.For<ITeamMember>(), Substitute.For<ITeamMember>())
            };

            sut.Pair1.Item1.Name.Returns("Adam");
            sut.Pair1.Item2.Name.Returns("Bertil");
            sut.Pair2.Item1.Name.Returns("Cecilia");
            sut.Pair2.Item2.Name.Returns("David");

            Assert.That(sut.ToString(), Is.EqualTo("Jul 13;Adam;Bertil;Cecilia;David"));
        }

        [TestCase(1, 2, 3, 4, true)]
        [TestCase(2, 1, 3, 4, true)]
        [TestCase(3, 4, 1, 2, true)]
        [TestCase(4, 3, 2, 1, true)]
        [TestCase(0, 2, 3, 4, false)]
        [TestCase(1, 3, 2, 4, false)]
        [TestCase(1, 2, 0, 4, false)]
        public void ShouldBeEquivalentWhenPairsAreTheSame(int player1, int player2, int player3, int player4, bool expextedResult)
        {
            var players = new[] { Substitute.For<ITeamMember>()
            ,Substitute.For<ITeamMember>()
            ,Substitute.For<ITeamMember>()
            ,Substitute.For<ITeamMember>()
            ,Substitute.For<ITeamMember>()};

            var sut = new Monday
            {
                Date = new DateTime(2019, 07, 13),
                Pair1 = (players[1], players[2]),
                Pair2 = (players[3], players[4])
            };

            var equivalent = new Tuesday
            {
                Date = new DateTime(2019, 07, 14),
                Pair1 = (players[player1], players[player2]),
                Pair2 = (players[player3], players[player4])
           };

            Assert.That(sut.IsEquivalent(equivalent), Is.EqualTo(expextedResult));
        }

        [Test]
        public void ShouldBePlannedOnlyWhenBothPairsAreFull()
        {
            var sut = new Monday();
            Assert.That(sut.IsPlanned, Is.False);

            sut.Pair1 = (Substitute.For<ITeamMember>(), Substitute.For<ITeamMember>());
            Assert.That(sut.IsPlanned, Is.False);

            sut.Pair2 = (Substitute.For<ITeamMember>(), Substitute.For<ITeamMember>());
            Assert.That(sut.IsPlanned, Is.True);

            sut.Pair1 = (Substitute.For<ITeamMember>(), null);
            Assert.That(sut.IsPlanned, Is.False);
        }
    }
}
