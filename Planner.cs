﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SangvinikernaLottning
{
    class Planner
    {
        public Random Random { get; set; }

        public void FillSchedule(ISchedule schedule)
        {
            var randomizedDays = schedule.Days.ToList().Shuffle(Random);

            foreach (var day in randomizedDays)
            {
                FindBestPairForDay(day, 1, schedule);
                FindBestPairForDay(day, 2, schedule);
            }
        }

        public void ImproveByReplacingPlayers(ISchedule schedule)
        {
            var allAssignments = GetAllPlayerAsssignments(schedule).ToList().Shuffle(Random);

            while (ImproveByReplacingPlayer(allAssignments, schedule))
                allAssignments = GetAllPlayerAsssignments(schedule).ToList().Shuffle(Random);
        }

        public void ImproveBySwitchingPlayers(ISchedule schedule)
        {
            var allAssignments = GetAllPlayerAsssignments(schedule).ToList().GetAllPairs().ToList().Shuffle(Random);

            while (ImproveBySwitchingPlayer(allAssignments, schedule))
                allAssignments = GetAllPlayerAsssignments(schedule).ToList().GetAllPairs().ToList().Shuffle(Random);
        }

        public void ImproveBySwithingPairs(ISchedule schedule)
        {
            var allAssignments = GetAllPairAsssignments(schedule).ToList().GetAllPairs().ToList().Shuffle(Random);

            while (ImproveBySwitchingPair(allAssignments, schedule))
                allAssignments = GetAllPairAsssignments(schedule).ToList().GetAllPairs().ToList().Shuffle(Random);
        }

        private void FindBestPairForDay(IPlayingDay day, int pairNumber, ISchedule schedule)
        {
            AssignPairToDay(day, pairNumber, (null, null));
            var initailScores = schedule.TeamMembers.ToDictionary(m => m, m => m.GetBrokenRuleScore());
            var candidates = new List<((ITeamMember, ITeamMember) Pair, int Gain)>();

            foreach (var pair in schedule.TeamMembers.GetAllPairs())
            {
                AssignPairToDay(day, pairNumber, pair);
                if (pair.Item1.IsSchedulePossible() && pair.Item2.IsSchedulePossible())
                {
                    var newScore = pair.Item1.GetBrokenRuleScore() + pair.Item2.GetBrokenRuleScore();
                    var gain = initailScores[pair.Item1] + initailScores[pair.Item2] - newScore;
                    candidates.Add((pair, gain));
                }

                AssignPairToDay(day, pairNumber, (null, null));
            }

            if (candidates.Count == 0)
                throw new NoFeasibleScheduleFound();

            var topCandidates = candidates.GroupBy(c => c.Gain).OrderByDescending(g => g.Key).First();

            AssignPairToDay(day, pairNumber, topCandidates.ToList().RandomItem(Random).Pair);
        }

        private void AssignPairToDay(IPlayingDay day, int pairNumber, (ITeamMember, ITeamMember) pair)
        {
            if (pairNumber == 1)
                day.Pair1 = pair;
            else
                day.Pair2 = pair;
        }

        private IEnumerable<(Action<ITeamMember> Assign, ITeamMember InitialValue)> GetAllPlayerAsssignments(ISchedule schedule)
        {
            foreach(var day in schedule.Days)
            {
                yield return (member => day.Pair1 = (member, day.Pair1.Item2), day.Pair1.Item1);
                yield return (member => day.Pair1 = (day.Pair1.Item1, member), day.Pair1.Item2);
                yield return (member => day.Pair2 = (member, day.Pair2.Item2), day.Pair2.Item1);
                yield return (member => day.Pair2 = (day.Pair2.Item1, member), day.Pair2.Item2);
            }
        }

        private IEnumerable<(Action<(ITeamMember, ITeamMember)> Assign, (ITeamMember, ITeamMember) InitialValue)> GetAllPairAsssignments(ISchedule schedule)
        {
            foreach (var day in schedule.Days)
            {
                yield return (pair => day.Pair1 = pair, day.Pair1);
                yield return (pair => day.Pair2 = pair, day.Pair2);
            }
        }


        private bool ImproveByReplacingPlayer(IList<(Action<ITeamMember> Assign, ITeamMember InitialValue)> allAssignments, ISchedule schedule)
        {
            var initialScore = schedule.GetTotalScore();

            foreach(var assignement in allAssignments)
            {
                foreach(var teamMember in schedule.TeamMembers)
                {
                    assignement.Assign(teamMember);

                    if (schedule.IsPossible() && schedule.GetTotalScore() < initialScore)
                    {
                        return true;
                    }
                }

                assignement.Assign(assignement.InitialValue);
            }

            return false;
        }

        private bool ImproveBySwitchingPlayer(IList<((Action<ITeamMember> Assign, ITeamMember InitialValue), (Action<ITeamMember> Assign, ITeamMember InitialValue))> allAssignmentPairs, ISchedule schedule)
        {
            var initialScore = schedule.GetTotalScore();

            foreach (var assignement in allAssignmentPairs)
            {
                assignement.Item1.Assign(assignement.Item2.InitialValue);
                assignement.Item2.Assign(assignement.Item1.InitialValue);

                if (schedule.IsPossible() && schedule.GetTotalScore() < initialScore)
                {
                    return true;
                }

                assignement.Item1.Assign(assignement.Item1.InitialValue);
                assignement.Item2.Assign(assignement.Item2.InitialValue);
            }

            return false;
        }

        private bool ImproveBySwitchingPair(IList<((Action<(ITeamMember, ITeamMember)> Assign, (ITeamMember, ITeamMember) InitialValue), (Action<(ITeamMember, ITeamMember)> Assign, (ITeamMember, ITeamMember) InitialValue))> allAssignmentPairs, ISchedule schedule)
        {
            var initialScore = schedule.GetTotalScore();

            foreach (var assignement in allAssignmentPairs)
            {
                assignement.Item1.Assign(assignement.Item2.InitialValue);
                assignement.Item2.Assign(assignement.Item1.InitialValue);

                if (schedule.IsPossible() && schedule.GetTotalScore() < initialScore)
                {
                    return true;
                }

                assignement.Item1.Assign(assignement.Item1.InitialValue);
                assignement.Item2.Assign(assignement.Item2.InitialValue);
            }

            return false;
        }
    }
}
